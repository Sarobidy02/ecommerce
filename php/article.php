<?php 
    class article{
        private $_id;
        private $_nom;
        private $_photo;
        private $_quantite;
        private $_prix;
        private $_reduction;

        public function __construct($_id, $_nom, $_photo, $_prix, $_reduction, $_quantite){
            $this->_id = $_id;
            $this->_nom = $_nom;
            $this->_photo = $_photo;
            $this->_prix = $_prix;
            $this->_reduction = $_reduction;
            $this->_quantite = $_quantite;
        }
        public function get_id(){
            return $this->_id;
        }
        public function get_nom(){
            return $this->_nom;
        }
        public function get_photo(){
            return $this->_photo;
        }
        public function get_prix(){
            return $this->_prix * $this->_quantite;
        }
        public function get_reduction(){
            return $this->_reduction;
        }
        public function get_quantite(){
            return $this->_quantite;
        }
        public function set_quantite($_quantite){
            $this->_quantite = $_quantite;
        }
    }
?>