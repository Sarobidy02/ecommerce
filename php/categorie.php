<?php 
    include('function.php'); 
    include('panier.php');
    session_start();
    $action = $_GET['action'];
    if($action == "panier"){
        if(!isset($_SESSION['panier'])){
            $_SESSION["panier"] = new panier();
        }
    }
    if($action == "add"){
        if(!isset($_SESSION['panier'])){
            $_SESSION["panier"] = new panier();
        }
        $article = new article($_GET['id'], $_GET['nom'], $_GET['photo'], $_GET['prix'], $_GET['reduction'], $_GET['quantite']);
        $_SESSION["panier"]->ajouter($article);
    }else if($action == "supr"){
        $_SESSION["panier"]->set_panier(remove_from_tab($_SESSION["panier"]->get_panier(), $_GET['pos']));
    }else if($action == "vider"){
        $_SESSION['panier']->vider();
    }else if($action == "recherche"){
        $produit = recherche($bdd, $_GET['recherche']);
        $action = "categorie";
    }
    $total = 0;
?>
<!DOCTYPE html>
<html>
    <head>
        <title>categorie</title>
        <!-- Required meta tags -->
        <meta charset=”UTF-8” />
        <!-- make it responsive -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- my CSS -->
        <link href="../CSS/bootstrap.css" rel="stylesheet">
        <link href="../CSS/bootstrap.min.css" rel="stylesheet">
        <link href="../CSS/Css.css?version=51" type="text/css" rel="stylesheet">
    </head>
    <body>
       <?php 
            include('head.php'); 
            if(isset($_GET['id_categorie'])){
                $produit = select_Categorie($bdd, $_GET['id_categorie']);
            }
            
       ?>
        <div class="container-fluid">
            <div class="row">
                <div id="categoriecontent" class="col">
                    <div class="row">
                        <div id="categorie" class="col-md-8">
                            <div class="row">
                                <div class="col">
                                    <?php if($action == "categorie" && count($produit)== 0){?>
                                        <div id="vide" class="col">
                                            <div class="row">
                                                <div id="panier_vide" class="col">
                                                    <h1 class="panier_text">NO RESULT</h1>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <svg id="cart" class="bi bi-cart2" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l1.25 5h8.22l1.25-5H3.14zM5 13a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <table class="table">
                                        <tbody>
                                            <?php 
                                            if($action == "categorie"){
                                                for($i=0; $i<count($produit); $i++){ ?>
                                            <tr>
                                                <td>
                                                    <div id="photo_categorie" class="col">
                                                        <a href=<?php echo "produit.php?id_Produit=".$produit[$i]['id_Produit'];?>><img src=<?php echo "../image/".$produit[$i]['Photo'];?> alt="..." class="img-thumbnail"></a>
                                                    </div>
                                                </td>
                                                <td><?php echo $produit[$i]['Nom_Produit']; ?></td>
                                                <td><?php echo $produit[$i]['Prix']; ?></td>
                                            </tr>
                                            <!-- tout ce qui concerne le panier affichage suppression -->
                                            <?php 
                                                }
                                            }else{
                                                for($i=0; $i<count($_SESSION['panier']->get_panier()); $i++){
                                                    $total = $total + $_SESSION['panier']->get_panier()[$i]->get_prix();
                                                ?>
                                            <tr>
                                                <td>
                                                    <div id="photo_categorie" class="col">
                                                        <img src=<?php echo "../image/".$_SESSION['panier']->get_panier()[$i]->get_photo();?> alt="..." class="img-thumbnail">
                                                    </div>
                                                </td>
                                                <td><?php echo $_SESSION['panier']->get_panier()[$i]->get_nom(); ?></td>
                                                <td><?php echo $_SESSION['panier']->get_panier()[$i]->get_quantite(); ?></td>
                                                <td><?php echo "$".$_SESSION['panier']->get_panier()[$i]->get_prix(); ?></td>
                                                <td><button type="button" class="btn btn-success">ACHETER</button></td>
                                                <td><a href=<?php echo "categorie.php?action=supr&pos=".$i; ?>><button type="button" class="btn btn-danger">SUPPRIMER</button></a></td>
                                            </tr>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <?php if($action!="categorie"){
                                        if(count($_SESSION['panier']->get_panier())!=0){
                                ?>
                                <div class="col">
                                    <div class="row"> 
                                        <div id="total" class="col-md-8">
                                            <h1>TOTAL = <?php echo "$".$total; ?></h1>
                                        </div>
                                        <div id="vider" class="col">
                                            <div class="row">
                                                <div class="col"><button type="button" class="btn btn-primary">ACHETER</button></div>
                                                <div class="col"><a href="categorie.php?action=vider"><button type="button" class="btn btn-secondary">VIDER</button></a></div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <?php }?>
                                </div>
                                <?php if(count($_SESSION['panier']->get_panier())==0){ ?>
                                <div id="vide" class="col">
                                    <div class="row">
                                        <div id="panier_vide" class="col">
                                            <h1 class="panier_text">RIEN DANS LE PANIER</h1>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <svg id="cart" class="bi bi-cart2" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l1.25 5h8.22l1.25-5H3.14zM5 13a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('foot.php'); ?>
    </body>
</html>