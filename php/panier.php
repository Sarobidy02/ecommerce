<?php 
    include('article.php');
    class panier{
        private $_articles = array();

        public function get_index($id){
            for($i=0; $i<count($this->_articles); $i++){
                if($this->_articles[$i]->get_id() == $id){
                    return $i;
                }
            }
            return -1;
        }
        public function ajouter($produit){
            $index = $this->get_index($produit->get_id());
            if($index < 0){
                array_push($this->_articles, $produit);
            }else{
                $this->_articles[$index]->set_quantite($produit->get_quantite()+$this->_articles[$index]->get_quantite());
            }
        }
        public function supprimer($index){
            array_splice($this->_articles, $index, $index);
        }
        public function get_panier(){
            return $this->_articles;
        }
        public function set_panier($tab){
            $this->_articles = $tab;
        }
        public function vider(){
            $this->_articles = array();
        }
    }
?>