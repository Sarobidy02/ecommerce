<?php 
    include('function.php'); 
    session_start();
    if(!isset($_SESSION['nom'])){
        header('Location: ../index.php');
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Produits</title>
        <!-- Required meta tags -->
        <meta charset=”UTF-8” />
        <!-- make it responsive -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- my CSS -->
        <link href="../CSS/bootstrap.css" rel="stylesheet">
        <link href="../CSS/bootstrap.min.css" rel="stylesheet">
        <link href="../CSS/Css.css?version=51" type="text/css" rel="stylesheet">
    </head>
    <body>
       <?php include('head.php'); ?>
        <div class="container-fluid">
            <div class="row">
                <div id="homecontent" class="col">
                    <div class="row">
                        <div id="scrol" class="col">
                            <img class="d-block w-100" src="../image/apple.jpg" alt="First slide">
                        </div>
                    </div>
                    <div class="row">
                        <div id="produit" class="col-md-8">
                            <div class="row">
                                <div id="titreHome" class="col-md-6">
                                    <h1>CATÉGORIES POPULAIRE</h1>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                <?php 
                                $listeCategorie = getCategorie($bdd); 
                                for($i=0; $i<count($listeCategorie); $i++){ ?>

                                        <div id="carteCategorie" class="card" style="width: 18rem;">
                                            <a href=<?php echo "categorie.php?id_categorie=".$listeCategorie[$i]['id_Categorie']."&action=categorie";?>><img class="card-img-top" src=<?php echo "../image/".$listeCategorie[$i]['Photo']; ?> alt="Card image cap"></a>
                                            <div  class="card-body">
                                                <p class="card-text"><?php echo $listeCategorie[$i]['Nom_Categorie']; ?></p>
                                            </div>
                                        </div>

                                <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <?php include('foot.php'); ?>
        <script src="../JS/bootstrap.js"></script>
    </body>
</html>