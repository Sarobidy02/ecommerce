<?php 
    require('connexion.php');
    function fetch_result($donne){
        while($temp = mysqli_fetch_assoc($donne)){
            $tab[] = $temp;
        }
        return $tab;
    }
    function getCategorie($connexion){
        $requete = "select * from Categorie";
        $requete = mysqli_query($connexion, $requete);
        $result = fetch_result($requete);
        mysqli_free_result($requete);
        return $result;
    }
    function select_Categorie($connexion, $categorie){
        $requete = "select * from Produit where id_Categorie='%d'";
        $requete = sprintf($requete, $categorie);
        $requete = mysqli_query($connexion, $requete);
        if($requete->num_rows == 0)return array();
        $result = fetch_result($requete);
        mysqli_free_result($requete);
        return $result;
    }
    function select_product($connexion, $id){
        $requete = "select * from Produit where id_Produit='%d'";
        $requete = sprintf($requete, $id);
        $requete = mysqli_query($connexion, $requete);
        $result = fetch_result($requete);
        return $result;
    }
    function remove_first($tab){
        $result = array();
        for($i=1; $i<count($tab); $i++){
            array_push($result, $tab[$i]);
        }
        return $result;
    }
    function remove_from_tab($tab, $index){
        if($index == 0){
            if(count($tab) == 1){
                return array();
            }
            return remove_first($tab);
        }
        array_splice($tab, $index, $index);
        return $tab;
    }
    function recherche($connexion, $mot_clee){
        $requete = "select * from Produit where Nom_Produit like '%".$mot_clee."%'";
        $requete = mysqli_query($connexion, $requete);
        if($requete->num_rows == 0)return array();
        $result = fetch_result($requete);
        mysqli_free_result($requete);
        return $result;
    }
    function is_user($connexion, $name, $pass){
        $requete = "select count(*) from User where Nom='%s' and Pass='%s'";
        $requete = sprintf($requete, $name, $pass);
        echo $requete;
        $requete = mysqli_query($connexion, $requete);
        $result = mysqli_fetch_assoc($requete);
        $teste = $result["count(*)"];
        if($teste == 0)return false;
        return true;
    }
?>