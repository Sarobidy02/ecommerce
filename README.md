#e-commerce 
---
**Version 1.0.0**

Vous êtes à la recherche d’appareils numériques ou des vêtements, vous pouvez effectuer ces achats sur ce site.

- login et déconnexion
- recherche de produit par nom
- pages dynamiques pour le produit sélectionné
- gestion de panier (ajouter, supprimer, vider)
- finaliser la commande

Pour tester veuillez charger les données qui se trouvent dans sql/base.sql , (user name = Saro , mdp = user)

Technologies:

- HTML, CSS, Boostrap
- PHP, SQL

Attention !! Cette version du code ne prend pas encore compte l'injection sql. 

---

## License & copyright
© Sarobidy RAPETERAMANA
