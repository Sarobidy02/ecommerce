<?php 
    include('php/function.php');
    if(isset($_GET['login'])){
        $teste = is_user($bdd, $_GET['name'], $_GET['pass']);
        if($teste){
            header('Location: php/home.php');
            session_start();
            $_SESSION['nom'] = $_GET['name'];
        }else{
            header('Location: index.php?erreur=true');
        }
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Produits</title>
        <!-- Required meta tags -->
        <meta charset=”UTF-8” />
        <!-- make it responsive -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- my CSS -->
        <link href="CSS/bootstrap.css" rel="stylesheet">
        <link href="CSS/bootstrap.min.css" rel="stylesheet">
        <link href="CSS/Css.css?version=51" type="text/css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <form action="index.php" method="get">
                        <div class="form-group">
                            <label for="exampleInputEmail1">User Name</label>
                            <input name="name" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input placeholder">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input name="pass" type="password" class="form-control" id="exampleInputPassword1">
                            <input name="login" type="hidden" value="log">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
            <div class=row>
                <div class="col">
                    <?php if(isset($_GET['erreur'])) echo "<p>Mauvais mots de passe ou login</p>"; ?>
                </div>
            </div>
        </div>
    </body>
</html>