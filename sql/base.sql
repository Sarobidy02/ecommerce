create database ecommerce;
use ecommerce;
create table Categorie(
    id_Categorie INTEGER(5),
    Nom_Categorie VARCHAR(20),
    Photo VARCHAR(20),
    primary key(id_Categorie)
);
create table Produit(
    id_Produit INTEGER(5),
    Nom_Produit VARCHAR(20),
    Descr VARCHAR(500),
    Photo VARCHAR(20),
    Prix DECIMAL(10,2),
    Reduction INTEGER(2),
    id_Categorie INTEGER(5),
    foreign key(id_Categorie) references Categorie(id_Categorie)
);
create table User(
    id_user INTEGER(5),
    Nom VARCHAR(20),
    Email VARCHAR(30),
    Pass VARCHAR(10)
);
insert into User values('0','Saro','mon_mail@gmail.com','user');
insert into Categorie values('0', 'Hight-tech', 'iPad_Pro.jpg');
insert into Categorie values('1', 'Informatique', 'asus_rog.jpg');
insert into Categorie values('2', 'Vetement', 'north_faceb.jpg');
insert into Categorie values('3', 'Manga', 'manga.jpg');

insert into Produit values('0','xiaomi-mi 10','Nous entrons dans une nouvelle ère avec la démocratisation de la 5G, pour faire face à ce nouveau challenge technologie Xiaomi a équipé le Mi 10 d’un modem X55 pour profiter d’une connectivité 5G très rapide. Ce modem vous donne également la possibilité de vous connecter simultanément à trois réseaux.','xiaomi.jpg','799','0','0');
insert into Produit values('1','oneplus 7 Pro','Incroyablement immersif, Ecran 6.67\'\' Fluid AMOLED 90Hz QHD+. Triple capteur photo pour chaque instant, 48MP, grand angle, zoom optique x3. Une Puissance sans précédent, Snapdragon™ 855, jusqu\'à 12GB RAM. Prêt en 20 min, Charge rapide Warp Charge 30, 4000 mAh
. Expériences audio & gaming exceptionnelles, double haut-parleur stéréo Atmos Dolby','OnePlus-7-Pro.jpg','580','0','0');
insert into Produit values('2','samsung S10','Mémoire RAM : 8 Go, Double port SIM (nano SIM), Ecran : Quad HD+ 550 ppp, Super AMOLED 16 M couleurs, Norme HDR10+, Gorilla Glass 6, Filtre de lumière bleue, Always on Display, Lecteur d\'empreinte digitale ultrasonique, Bords incurvés avant et arrière, IP : Certification IP68, Modes app. Principal : AR Emoji, Alimentation, Panorama, Pro, Mode Portrait, Photo, Vidéo, Super ralenti, Ralenti, Hyperlapse, Modes caméra frontale  : AR Emoji, Mode Portrait, Photo, Vidéo,','samsung_S10.jpg','499','0','0');
insert into Produit values('3','iphone 11 Pro','Mobile sous OS Apple - iOS 13 - 4G+. Ecran tactile 14,7 cm (5,8 pouces) 2436 x 1125 pixels. Processeur A13 Bionic - Capacité 64 Go. Triple appareil photo ultra grand-angle, grand-angle et téléobjectif 12 Mpx','iphone_11_Pro.jpg','1159','0','0');

insert into Produit values('4','Moniteur LED IPS','Moniteur LED IPS 27’’ 16:9. Résolution FHD 1920x1080 HDMI, DisplayPort. NVIDIA Gsync ™ Compatible, AMD FreeSync™. Pied rotatif','lg_pc.jpg','329','0','0');
insert into Produit values('5','LG 55UM7450','Le téléviseur LG 55UM7450 embarque une dalle LCD LED UHD 4K de 55" (139 cm) de diagonale d\'une résolution de 3840 x 2160 pixels. Il intègre un processeur Quad Core, dispose d\'un rétroéclairage Direct LED et prend en charge les contenus vidéo à large plage dynamique HDR-10 et HLG pour produire une image à la fois claire et détaillée.','tv_lg.jpg','539','0','0');
insert into Produit values('6','LG nanoGC','Le téléviseur LG 55UM7450 embarque une dalle LCD LED UHD 4K de 55" (139 cm) de diagonale d\'une résolution de 3840 x 2160 pixels. Il intègre un processeur Quad Core, dispose d\'un rétroéclairage Direct LED et prend en charge les contenus vidéo à large plage dynamique HDR-10 et HLG pour produire une image à la fois claire et détaillée.','lg_nanoGC.jpg','439','0','0');
insert into Produit values('7','LG TV 86pouces','Le téléviseur LG 55UM7450 embarque une dalle LCD LED UHD 4K de 55" (139 cm) de diagonale d\'une résolution de 3840 x 2160 pixels. Il intègre un processeur Quad Core, dispose d\'un rétroéclairage Direct LED et prend en charge les contenus vidéo à large plage dynamique HDR-10 et HLG pour produire une image à la fois claire et détaillée.','lg_tv_86 pouces.jpg','399','0','0');

insert into Produit values('8','Spirit of gamer','Le casque PRO-H5 Spirit Of Gamer vous propose une immersion totale dans l\'univers du Gaming. D\'une conception circum-aural, le casque est également équipé d\'un micro adapté à toutes les situations.','gaming1.jpg','39','12','0');
insert into Produit values('9','logitec g935','Le casque PRO-H5 Spirit Of Gamer vous propose une immersion totale dans l\'univers du Gaming. D\'une conception circum-aural, le casque est également équipé d\'un micro adapté à toutes les situations.','logitec_g935.jpg','50','12','0');
insert into Produit values('10','SteelSeries','Le casque PRO-H5 Spirit Of Gamer vous propose une immersion totale dans l\'univers du Gaming. D\'une conception circum-aural, le casque est également équipé d\'un micro adapté à toutes les situations.','SteelSeries.jpg','49','12','0');
insert into Produit values('11','HyperX Cloud','Le casque PRO-H5 Spirit Of Gamer vous propose une immersion totale dans l\'univers du Gaming. D\'une conception circum-aural, le casque est également équipé d\'un micro adapté à toutes les situations.','HyperX_Cloud.jpg','60','12','0');

insert into Produit values('12','LDLC Bazoka','Le LDLC PC Bazooka est un PC Gaming abordable équipé d\'un processeur Intel dernière génération, d\'une carte mère MSI, d\'un SSD PCIe NVMe 480 Go et d\'une carte graphique Nvidia GeForce RTX 2060.','LDLC_Bazoka.jpg','1299','4','1');
insert into Produit values('13','assus R509JA','Le LDLC PC Bazooka est un PC Gaming abordable équipé d\'un processeur Intel dernière génération, d\'une carte mère MSI, d\'un SSD PCIe NVMe 480 Go et d\'une carte graphique Nvidia GeForce RTX 2060.','assus_R509JA.jpg','1499','4','1');
insert into Produit values('14','asus rog','Le LDLC PC Bazooka est un PC Gaming abordable équipé d\'un processeur Intel dernière génération, d\'une carte mère MSI, d\'un SSD PCIe NVMe 480 Go et d\'une carte graphique Nvidia GeForce RTX 2060.','asus_rog.jpg','1599','4','1');
insert into Produit values('15','macbook pro','Le LDLC PC Bazooka est un PC Gaming abordable équipé d\'un processeur Intel dernière génération, d\'une carte mère MSI, d\'un SSD PCIe NVMe 480 Go et d\'une carte graphique Nvidia GeForce RTX 2060.','macbook_pro.jpg','1600','4','1');

insert into Produit values('16','samsung tab','De par ses paramètres avancés, la Galaxy Tab S5e 64 Go Wifi Samsung Noir est gage d’efficacité sans faille. Cette tablette est basée sur Android 9.0 Pie, ce qui la rend compatible avec les applications dernièrement inventées. Elle embarque un processeur octa-core, dont 2 cœurs à fréquence de 2 GHz et 6 cœurs de 1,7 GHz. Et grâce à ses 4 Go de mémoire vive, cet appareil assure une expérience d’utilisation fluide et sans latence.','samsung_tab.jpg','429','4','1');
insert into Produit values('17','iPad Pro','De par ses paramètres avancés, la Galaxy Tab S5e 64 Go Wifi Samsung Noir est gage d’efficacité sans faille. Cette tablette est basée sur Android 9.0 Pie, ce qui la rend compatible avec les applications dernièrement inventées. Elle embarque un processeur octa-core, dont 2 cœurs à fréquence de 2 GHz et 6 cœurs de 1,7 GHz. Et grâce à ses 4 Go de mémoire vive, cet appareil assure une expérience d’utilisation fluide et sans latence.','iPad_Pro.jpg','600','4','1');
insert into Produit values('18','samsung tab18','De par ses paramètres avancés, la Galaxy Tab S5e 64 Go Wifi Samsung Noir est gage d’efficacité sans faille. Cette tablette est basée sur Android 9.0 Pie, ce qui la rend compatible avec les applications dernièrement inventées. Elle embarque un processeur octa-core, dont 2 cœurs à fréquence de 2 GHz et 6 cœurs de 1,7 GHz. Et grâce à ses 4 Go de mémoire vive, cet appareil assure une expérience d’utilisation fluide et sans latence.','samsung_tab18.jpg','529','4','1');
insert into Produit values('19','Huawei pad','De par ses paramètres avancés, la Galaxy Tab S5e 64 Go Wifi Samsung Noir est gage d’efficacité sans faille. Cette tablette est basée sur Android 9.0 Pie, ce qui la rend compatible avec les applications dernièrement inventées. Elle embarque un processeur octa-core, dont 2 cœurs à fréquence de 2 GHz et 6 cœurs de 1,7 GHz. Et grâce à ses 4 Go de mémoire vive, cet appareil assure une expérience d’utilisation fluide et sans latence.','Huawei_pad.jpg','430','4','1');

insert into Produit values('20','Geforce RTX','La Carte est équipée de 2 ventilateurs de 90mm et de 2 caloducs de hautes performances. Ils peuvent transférer plus de puissance pour améliorer la température de la carte graphique. La Fonction OC en 1 Clic s’exécute avec un simple bouton qui optimise les paramètres d’overclocking pour plus de puissance tout en gardant une stabilité efficace durant vos sessions de jeu.','Geforce_RTX.jpg','451','10','1');
insert into Produit values('21','GIGABYTE RTX','La Carte est équipée de 2 ventilateurs de 90mm et de 2 caloducs de hautes performances. Ils peuvent transférer plus de puissance pour améliorer la température de la carte graphique. La Fonction OC en 1 Clic s’exécute avec un simple bouton qui optimise les paramètres d’overclocking pour plus de puissance tout en gardant une stabilité efficace durant vos sessions de jeu.','GIGABYTE_RTX.jpg','460','10','1');
insert into Produit values('22','EVGA-RTX','La Carte est équipée de 2 ventilateurs de 90mm et de 2 caloducs de hautes performances. Ils peuvent transférer plus de puissance pour améliorer la température de la carte graphique. La Fonction OC en 1 Clic s’exécute avec un simple bouton qui optimise les paramètres d’overclocking pour plus de puissance tout en gardant une stabilité efficace durant vos sessions de jeu.','EVGA-RTX.jpg','395','10','1');
insert into Produit values('23','MSI RTX','La Carte est équipée de 2 ventilateurs de 90mm et de 2 caloducs de hautes performances. Ils peuvent transférer plus de puissance pour améliorer la température de la carte graphique. La Fonction OC en 1 Clic s’exécute avec un simple bouton qui optimise les paramètres d’overclocking pour plus de puissance tout en gardant une stabilité efficace durant vos sessions de jeu.','MSI_RTX.jpg','500','10','1');

insert into Produit values('24','north face','J\'adore la mettre quand il pleut , elle me réchauffe et est confortable et très fonctionnelle','north_face.jpg','250','25','2');
insert into Produit values('25','north face noir','J\'adore la mettre quand il pleut , elle me réchauffe et est confortable et très fonctionnelle','north_face_noir.jpg','196','25','2');
insert into Produit values('26','north face cool','J\'adore la mettre quand il pleut , elle me réchauffe et est confortable et très fonctionnelle','north_faceb.jpg','250','25','2');
insert into Produit values('27','north face femme','J\'adore la mettre quand il pleut , elle me réchauffe et est confortable et très fonctionnelle','north_face_f.jpg','99','25','2');

insert into Produit values('28','air force easter','L\'éclat ne ternit pas avec la Nike Air Force 1 LV8 Utility. Ce modèle original du basketball apporte un nouveau souffle à ses éléments les plus appréciés : le cuir impeccable, les couleurs classiques et la parfaite quantité d\'éclat pour vous permettre de briller.','air_force_easter.jpg','170','25','2');
insert into Produit values('29','jordan1','Toujours aussi tendance et jamais dépassée, la Air Jordan 1 Low est l\'une des sneakers les plus emblématiques de tous les temps. Cette version SE réinvente le modèle classique avec de nouveaux coloris et éléments de bordure stylés.','jordan1.jpg','200','25','2');
insert into Produit values('30','lebron 17','J\'adore la mettre quand il pleut , elle me réchauffe et est confortable et très fonToujours aussi tendance et jamais dépassée, la Air Jordan 1 Low est l\'une des sneakers les plus emblématiques de tous les temps. Cette version SE réinvente le modèle classique avec de nouveaux coloris et éléments de bordure stylés.ctionnelle','lebron_17.jpg','250','25','2');
insert into Produit values('31','dr martens','J\'adore la mettre quand il pleut , elle me réchauffe et est confortable et très fonctionnelle','dr_martens.jpg','120','25','2');

